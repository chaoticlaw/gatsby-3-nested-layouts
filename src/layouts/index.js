import React from 'react';
import Link from 'gatsby-link';
import styles from './index.module.css';

const ListLink = props =>
  <li style={{ display: `inline-block`, marginRight: `1rem`}}>
    <Link to={props.to}>
      {props.children}
    </Link>
  </li>;

export default ({children}) => (
  <div style={{ margin: `0 auto`, maxWidth: 650, padding: '1.25rem 1rem'}}>
    <Link to="#content" className={styles.jumptomain}>Go to Content</Link>
    <header style={{marginBottom: `1.5rem`}}>
      <Link to="/" style={{ textShadow: `none`, backgroundImage: `none`}}>
        <h3 style={{display: `inline`}}>My Sweet Site</h3>
      </Link>
      <ul style={{ listStyle: `none`, float: `right`}}>
        <ListLink to="/">Home</ListLink>
        <ListLink to="/about">About</ListLink>
        <ListLink to="/contact">Contact</ListLink>
      </ul>
    </header>
    <main id="content" style={{minHeight: `100vh`}}>
      {children()}
    </main>
    <footer style={{background: `#eee`, borderTop: `1px solid #ccc`, minHeight: `4em`}}>
      <h3>My Sweet Site</h3>
      <p>My Sweet Site is a site powered by Gatsby, and it feels great!</p>
    </footer>
  </div>
);
