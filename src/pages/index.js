import React from "react"

export default () => (
  <div>
    <h1>Hi! I'm building a Gatsby site to showcase nested layouts!</h1>
    <p>Hopefully you'll see this, as it means I was successful in achieving this! I should hopefully understand a bit more the process behind designing and deploying React powered sites.</p>
  </div>
);
