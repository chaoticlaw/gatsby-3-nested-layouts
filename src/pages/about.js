import React from "react"

export default () => (
  <div>
    <h1>About Me</h1>
    <p>I've worked a lot with modern web technologies, including HTML5, CSS3, JavaScript, AJAX, and PHP. I'm hoping to put React and Gatsby into this list also.</p>
  </div>
);
