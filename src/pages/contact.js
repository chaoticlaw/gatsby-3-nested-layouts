import React from "react"

export default () => (
  <div>
    <h1>Let's Talk.</h1>
    <p>Drop me a line over the phone, or via e-mail: <a href='mailto:minh@mnguyen.io'>minh@mnguyen.io</a></p>
  </div>
);
