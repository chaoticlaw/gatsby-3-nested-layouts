module.exports = {
  plugins: [{
    resolve: `gatsby-plugin-typography`,
    options: {
      pathToConfigModule: `src/utils/typography.js`,
    },
  }],
  pathPrefix: '/gatsby-3-nested-layouts'
}
